import datetime
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from flask import Flask, render_template
from flask_restplus import Resource, Api, fields
from flask.helpers import make_response
from flask import send_file
from flask import request
from flask import jsonify
from flask_cors import CORS
import os
from collections import Counter


import json


app = Flask(__name__)
# Number of seconds to wait before files and images expire
#app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1
CORS(app)
api = Api(app)

#curl -d '{"IBM":10, "C":4, "AAPL":3}' -H "Content-Type: application/json" -X POST http://localhost:5000/buildPortfolio


@api.route('/buildPortfolio', methods=['POST'])
class BuildPortfolio(Resource):
    portfolio = {}
    def post(self):
        response = request.get_json()
        #Converts the json payload received into a python dictionary
        for i in response:
            key = i['id']
            val = i['quantity']
            self.portfolio[key] = val
        return ''

@api.route('/graph', methods=['GET'])
class GraphPortfolio(Resource):
    def combinePortfolio(self, portfolio):
        #Returns an empty dataframe if the portfolio is empty
        if not portfolio:
            return pd.DataFrame()
        #Picks out the first element of the dictionary to start building
        #a table with the date and closing price for that day
        first = list(portfolio.keys())[0]
        df = pdr.get_data_yahoo(first, '2020')
        df['Adj Close'] = df['Adj Close'] * 0
        df = df[['Adj Close']]
        for i in portfolio:
            #Gets the price data in 2020 and multiplies the closing price by the quantity
            dfi = pdr.get_data_yahoo(i, '2020')
            dfi['Adj Close'] = dfi['Adj Close'] * portfolio[i]
            dfi = dfi[['Adj Close']]
            #Need to merge the tables on the date to ensure that there is no empty data
            df = df.merge(dfi, on='Date')
        return df  

    def get(self):
        #Checks to see if dictionary is empty
        if not bool(BuildPortfolio.portfolio):
            print("Portfolio is empty")
            return send_file('image\\blankGraph.png', mimetype='image/png')
        print(BuildPortfolio.portfolio)
        #To avoid regraphing too many times, check the internal cache to see
        #if the request is identical to the previous one. If so, don't regraph.
        filesize = os.path.getsize("cache.txt")
        if filesize > 0:
            f = open("cache.txt", "r")
            line = f.read()
            f.close()
            if line == str(BuildPortfolio.portfolio):
                print("Caching")
                return send_file('image\\graph.png', mimetype='image/png')
            else:
                print("Not caching")
                f = open("cache.txt", "w")
                f.write(str(BuildPortfolio.portfolio))
                f.close()
        else:
            f = open("cache.txt", "w")
            f.write(str(BuildPortfolio.portfolio))
            f.close() 
        print("Re graphing")

        ret = self.combinePortfolio(BuildPortfolio.portfolio)
        if ret.empty:
            return send_file('image\\blankGraph.png', mimetype='image/png')
        #Sum the combined dataframe and save the result to a figure
        fig, ax = plt.subplots()
        summed_values = ret.sum(axis = 1, skipna = True)
        plt.plot(summed_values)
        fig.autofmt_xdate()
        ax.set_title('Portfolio Value')
        ax.set_xlabel('Date (Year/Month)')
        ax.set_ylabel('Price (USD)')
        plt.savefig('image/graph.png')
        plt.close('all')
        return send_file('image\\graph.png', mimetype='image/png')

@api.route('/chart', methods=['GET'])
class ChartPortfolio(Resource):
    def get(self):
        #Checks to see if dictionary is empty
        if not bool(BuildPortfolio.portfolio):
            print("Portfolio is empty")
            return send_file('image\\blankChart.png', mimetype='image/png')
        #Creates a pie chart for the portfolio
        counts = Counter(BuildPortfolio.portfolio)
        plt.pie([float(v) for v in counts.values()], labels=[k for k in counts],autopct=None)
        plt.title('Investment Preferences')
        plt.savefig('image/chart.png')
        print("Saving chart!")
        plt.close('all')
        return send_file('image\\chart.png', mimetype='image/png')   

    
if __name__ == '__main__':
    app.run(debug=True)
